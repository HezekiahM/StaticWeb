from django.shortcuts import render, get_object_or_404
from .models import Albums
from datetime import datetime


# Create your views here.
def index(request):
    all_albums = Albums.objects.all()
    context = {'all_albums': all_albums}
    return render(request, 'index.html', context=context)


def detail(request, album_id):
    album = get_object_or_404(Albums, pk=album_id)
    return render(request, "details.html", {'album':album})


def about(request):
    return render(request, 'about.html')
