from django.db import models


# Create your models here.
class Albums(models.Model):

    name = models.CharField(max_length=250)
    description = models.CharField(max_length=250)
    logo = models.CharField(max_length=300, default="https://images.pexels.com/photos/68147/waterfall-thac-dray-nur-buon-me-thuot-daklak-68147.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500")

    def __str__(self):
        return self.name + "-" + self.description


class Images(models.Model):

    albums = models.ForeignKey(Albums, on_delete=models.CASCADE)
    image_caption = models.CharField(max_length=250)
    image_link = models.CharField(max_length=250)

    def __str__(self):
        return self.image_caption
